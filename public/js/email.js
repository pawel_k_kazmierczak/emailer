var Email = {
    init: function () {
	this.initAction();
    },
    initAction: function () {
	$(document).on('click', '#send_mail', function () {
	    var button = $(this);
	    var html = $('#editor').html();
	    $('#newMessageContent').val(html);

	    var form = button.closest('form');
	    var data = form.serialize();
	    var url = form.data('url');
	    button.attr('disabled', 'disabled');

	    jQuery.post(url, data, function (data) {
		button.removeAttr('disabled');
		bootbox.alert(data.msg);

		if (data.success) {
		    form.find('.clean_after_send,#editor').html("").val("");
		}
	    });



	});
    }
}

Email.init();
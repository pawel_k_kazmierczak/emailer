<?php

namespace ApplicationTest;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

abstract class CommonTest extends AbstractHttpControllerTestCase {

    protected $traceError = true;
    protected $sm;

    public function setUp() {
	$this->setApplicationConfig(
		include 'C:/xampp_7_0_6/htdocs/emailer/config/application.config.php'
	);
	parent::setUp();

	$this->sm = $this->getApplicationServiceLocator();
    }

    public function arrayToParameters(array $post) {
	$parameters = new \Zend\Stdlib\Parameters($post);
	return $parameters;
    }

}

<?php

namespace Application\Controller\Factory;

use \Zend\ServiceManager\FactoryInterface;
use Application\Controller\IndexController;

class IndexFactory implements FactoryInterface {

    private $sm;

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
	$this->sm = $serviceLocator;
    }

    public function __invoke(\Interop\Container\ContainerInterface $container, $requestedName,
	    array $options = NULL) {

	$emailManager = $container->get(\Email\Manager\EmailManager::class);
	$controller = new IndexController();
	$controller->init($emailManager);
	return $controller;
    }

    protected function newInitObiect() {
	return new \Wspolne\Controller\Factory\InitObiekt();
    }

}

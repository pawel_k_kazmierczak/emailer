<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {

    private $emailManager;

    public function init(\Email\Manager\EmailManager $emailManager) {
	$this->emailManager = $emailManager;
    }

    public function indexAction() {
	return new ViewModel();
    }

    public function sendEmailAction() {
	$post = $this->getPost();

	$response = $this->emailManager->sendMessage($post);

	return new \Zend\View\Model\JsonModel($response->toArray());
    }

    protected function getPost() {
	$request = $this->getRequest();
	if ($request->isPost()) {
	    return $request->getPost();
	}
	return false;
    }

}

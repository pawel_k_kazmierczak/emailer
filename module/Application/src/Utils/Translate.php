<?php

namespace Application\Utils;

trait Translate {

    function translate($msg, array $params = []) {
	return vsprintf($msg, $params);
    }

    function translateConst($msg) {//TODO: Obsłużyć przypadek występowania zmiennych w msg (np. adresu email)
	switch ($msg) {
	    case 'API key authentication/authorization failure. You may be unauthorized to access the API or your API key may be expired. Visit API keys management section to check your keys.':
		return $this->translate('API key authentication/authorization failure. You may be unauthorized to access the API or your API key may be expired. Visit API keys management section to check your keys.');
	    default:
		return $msg;
    }
    }

}

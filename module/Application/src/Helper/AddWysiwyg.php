<?php

namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;

class AddWysiwyg extends AbstractHelper {

    public function __invoke($content = "") {
	return $this->view->partial('application/helper/wysiwyg.phtml', ['content' => $content]);
    }

}

<?php

namespace Email\Manager;

class EmailManager {

    use \Application\Utils\Translate;

    private $sm;
    private $result;
    private $title;
    private $content;
    private $from;
    private $to;
    private $failedRecipients;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$this->sm = $sm;
    }

    public function sendMessage($post) {

	$this->resetResul();
	try {
	    $this->sendMessageTry($post);
	} catch (\Exception $e) {
	    $this->result->setError($e->getMessage());
	}

	return $this->result;
    }

    public function parseParameters($post) {
	$this->content = $post->content;
	$this->title = $post->title;

	$this->from = $this->_parseEmail($post->from);
	$this->to = $this->_parseEmail($post->to);
    }

    public function doSend() {
	$transport = (new \Email\MailJet\Transport($this->sm));

	$mailer = new \Swift_Mailer($transport);

	$message = (new \Swift_Message($this->getTitle()))
		->setFrom($this->getFrom())
		->setTo($this->getTo())
		->setBody($this->getContent())
	;

	return $mailer->send($message, $this->failedRecipients);
    }

    public function checkFailedRecipients(array $failedRecipients) {

	if (count($failedRecipients) > 0) {
	    $this->result->addMsgs($failedRecipients);
	    throw new \Email\Exception\SendResultException($this->translate("Some errors occurrence"));
	}
    }

    public function checkStatus($status) {
	if (!$status) {
	    throw new \Email\Exception\SendResultException($this->translate("No emails send"));
	}
    }

    public function resetResul() {
	$this->result = new SendMailResult();
    }

    public function getFrom() {
	return $this->from;
    }

    public function getTo() {
	return $this->to;
    }

    public function getTitle() {
	return $this->title;
    }

    public function getContent() {
	return $this->content;
    }

    public function _parseEmail($email) {
	$mails = \Mail_RFC822::parseAddressList($email);
	$result = [];

	if ($mails instanceof \PEAR_Error) {
	    throw new \Email\Exception\ParseEmailException($this->translateConst($mails->message));
	}

	foreach ($mails as $mail) {
	    $result["{$mail->mailbox}@$mail->host"] = $mail->personal;
	}

	return $result;
    }

    private function sendMessageTry($post) {
	$this->resetFailedRecipients();
	$this->parseParameters($post);

	$status = $this->doSend();

	$this->checkFailedRecipients($this->failedRecipients);
	$this->checkStatus($status);
	$this->addSuccessMessage($status);
    }

    private function resetFailedRecipients() {
	$this->failedRecipients = [];
    }

    private function addSuccessMessage($number) {
	$this->result->addMsg($this->translate("%s emails has been sent", [$number]));
    }

}

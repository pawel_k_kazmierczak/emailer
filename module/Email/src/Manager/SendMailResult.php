<?php

namespace Email\Manager;

class SendMailResult {

    public $success = true;
    private $msgs = [];

    public static function create() {
	return new self();
    }

    public function toArray() {
	return ['success' => $this->success, 'msg' => implode(',', $this->msgs)];
    }

    public function setSuccess($success) {
	$this->success = $success;
	return $this;
    }

    public function addMsgs(array $msgs) {
	$this->msgs = array_merge($this->msgs, $msgs);
	return $this;
    }

    public function addMsg($msg) {
	$this->msgs[] = $msg;
	return $this;
    }

    public function setError($msg) {
	$this->addMsg($msg);
	$this->setSuccess(false);
    }

    public function getMsgs() {
	return $this->msgs;
    }

}

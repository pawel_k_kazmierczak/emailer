<?php

namespace Email;

class Module {

    public function getServiceConfig() {
	return array(
		'factories' => array(
			Manager\EmailManager::class => function($sm) {
			    return new Manager\EmailManager($sm);
			},
		),
	);
    }

}

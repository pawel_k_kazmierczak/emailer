<?php

namespace Email\MailJet;

class Transport implements \Swift_Transport {

    use \Application\Utils\Translate;

    private $appkey;
    private $secret_key;
    private $curl;
    private $data;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$config = $sm->get('config');
	$this->appkey = $config['email']['app_key'];
	$this->secret_key = $config['email']['secret_key'];
    }

    public function isStarted() {
	return $this->curl !== null;
    }

    public function start() {
	$this->curl = curl_init();
    }

    public function stop() {
	if ($this->isStarted()) {
	    curl_close($this->curl);
	    $this->curl = null;
	}
    }

    public function ping() {
	return $this->isStarted();
    }

    public function send(\Swift_Mime_SimpleMessage $message, &$failedRecipients = null) {
	try {
	    return $this->sendTry($message, $failedRecipients);
	} catch (\Email\Exception\AbstractEmailException $e) {
	    $failedRecipients[] = $this->translateConst($e->getMessage());
	    return 0;
	}
    }

    public function sendTry(\Swift_Mime_SimpleMessage $message, &$failedRecipients = null) {

	$this->prepareData($message);
	$this->setParameters();
	$response = $this->execute();
	$result = Response::fromJsonMsg($response, $failedRecipients);
	return $result->getStatus();
    }

    public function registerPlugin(\Swift_Events_EventListener $plugin) {

    }

    private function prepareData($message) {
	$mailjetMessages = Message::fromSwiftMessage($message);
	$this->data = json_encode(array(
		'Messages' =>
		$mailjetMessages
		)
	);
    }

    private function setParameters() {
	$url = "https://api.mailjet.com/v3.1/send";
	curl_setopt($this->curl, CURLOPT_POST, 1);
	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $this->data);
	curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($this->curl, CURLOPT_USERPWD, "{$this->appkey}:{$this->secret_key}");
	curl_setopt($this->curl, CURLOPT_URL, $url);
	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    }

    public function execute() {
	if (!$result = curl_exec($this->curl)) {
	    throw new \Email\Exception\CurlException($this->curl);
	}

	return $result;
    }

}

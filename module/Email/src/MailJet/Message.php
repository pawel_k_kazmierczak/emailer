<?php

namespace Email\MailJet;

class Message {

    public $From;
    public $To;
    public $Subject;
    public $HTMLPart;

    public static function fromSwiftMessage(\Swift_Mime_SimpleMessage $message) {

	$result = [];

	foreach ($message->getFrom() as $emailFrom => $nameFrom) {
	    foreach ($message->getTo() as $emailTo => $nameTo) {

		$mailJetMessage = new self();

		$mailJetMessage->From = ["Email" => $emailFrom, "Name" => $nameFrom];
		$mailJetMessage->To[] = ["Email" => $emailTo, "Name" => $nameTo];
		$mailJetMessage->Subject = $message->getSubject();
		$mailJetMessage->HTMLPart = $message->getBody();

		$result[] = $mailJetMessage;
	    }
	}

	return $result;
    }

}

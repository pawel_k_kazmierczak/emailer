<?php

namespace Email\MailJet;

use Email\Exception\ResponseParseException;

class Response {

    use \Application\Utils\Translate;

    private $status;

    public function getStatus() {
	return $this->status;
    }

    public static function fromJsonMsg($jsonMsg, &$failedRecipients) {
	$result = new Response();

	try {
	    $result->fromJsonMsgTry($jsonMsg, $failedRecipients);
	} catch (ResponseParseException $ex) {
	    $failedRecipients[] = $ex->getMessage();
	}

	return $result;
    }

    private function fromJsonMsgTry($jsonMsg, &$failedRecipients) {

	$msg = json_decode($jsonMsg);
	$this->checkError($msg);

	foreach ($msg->Messages as $email) {
	    if ($this->emailSuccess($email)) {
		$this->status++;
	    } else {
		$failedRecipients[] = $this->makeErrorForMessage($email);
	    }
	}
    }

    private function __construct() {
	$this->status = 0;
    }

    private function checkError($msg) {
	if (!$msg) {
	    throw new ResponseParseException($this->translate("Bad response format"));
	} else if (isset($msg->ErrorMessage)) {
	    throw new ResponseParseException($this->translateConst($msg->ErrorMessage));
	} else if (!isset($msg->Messages)) {
	    throw new ResponseParseException($this->translate("Bad response format:", [$msg]));
	}
    }

    private function emailSuccess($email) {
	return $email->Status === 'success';
    }

    private function makeErrorForMessage($object) {

	if (isset($object->Errors)) {
	    $result = [];
	    foreach ($object->Errors as $error) {
		$result[] = $this->translateConst($error->ErrorMessage);
	    }
	    return implode(",", $result);
	} else {
	    return $this->translate("Fail to send email:%s", [json_encode($object)]);
	}
    }

}

<?php

namespace EmailTest\MailJet;

class ResponseTest extends \ApplicationTest\CommonTest {

    private $failedRecipients;

    public function setUp() {
	parent::setUp();
	$this->failedRecipients = [];
    }

    public function test_parseResponse() {
	$response = \Email\MailJet\Response::fromJsonMsg('{"Messages":[{"Status":"success","CustomID":"","To":[{"Email":"pawel.rymsza@pastmo.pl","MessageUUID":"6ce50fb7-c842-413d-87f7-39091b96cd5f","MessageID":70650219412095977,"MessageHref":"https://api.mailjet.com/v3/REST/message/70650219412095977"}],"Cc":[],"Bcc":[]}]}',
			$this->failedRecipients);

	$this->assertEquals(1, $response->getStatus());
	$this->assertEmpty($this->failedRecipients);
    }

    public function test_parseResponse_two_messages() {
	$response = \Email\MailJet\Response::fromJsonMsg('{"Messages":[{"Status":"success"},{"Status":"success"}]}',
			$this->failedRecipients);

	$this->assertEquals(2, $response->getStatus());
	$this->assertEmpty($this->failedRecipients);
    }

    public function test_parseResponse_status_fail() {
	$response = \Email\MailJet\Response::fromJsonMsg('{"Messages":[{"Status":"fail","CustomID":"","To":[{"Email":"pawel.rymsza@pastmo.pl","MessageUUID":"6ce50fb7-c842-413d-87f7-39091b96cd5f","MessageID":70650219412095977,"MessageHref":"https://api.mailjet.com/v3/REST/message/70650219412095977"}],"Cc":[],"Bcc":[]}]}',
			$this->failedRecipients);

	$this->assertEquals(0, $response->getStatus());
	$this->assertNotEmpty($this->failedRecipients);
	$this->assertContains('pawel.rymsza@pastmo.pl', $this->failedRecipients[0]);
    }

    public function test_parseResponse_without_email() {
	$response = \Email\MailJet\Response::fromJsonMsg('{"Messages":[{"Status":"fail","CustomID":"","To":[],"Cc":[],"Bcc":[]}]}',
			$this->failedRecipients);

	$this->assertEquals(0, $response->getStatus());
	$this->assertNotEmpty($this->failedRecipients);
	$this->assertContains('Fail to send email', $this->failedRecipients[0]);
    }

    public function test_parseResponse_not_valid_response_null() {
	$this->checkError(null, 'Bad response format');
    }

    public function test_parseResponse_not_valid_response_empty_object() {
	$this->checkError("{}", 'Bad response format:');
    }

    public function test_error_authentication_authorization_failure() {
	$this->checkError('{"ErrorIdentifier":"fa20d48e-e35b-40fe-8db9-dcd9aad00464","StatusCode":401,"ErrorMessage":"API key authentication/authorization failure. You may be unauthorized to access the API or your API key may be expired. Visit API keys management section to check your keys."}'
		,
		'API key authentication/authorization failure. You may be unauthorized to access the API or your API key may be expired. Visit API keys management section to check your keys.');
    }

    public function test_error_blad_wiadomosci() {
	$this->checkError('{"Messages":[{"Errors":[{"ErrorIdentifier":"702c5bb0-c816-47f6-bb46-a7d4a81a710c","ErrorCode":"send-0008","StatusCode":403,"ErrorMessage":"\"kontaktp@pastmolll.pl\" is not an authorized sender email address for your account.","ErrorRelatedTo":["From"]}],"Status":"error"}]}'
		, '"kontaktp@pastmolll.pl" is not an authorized sender email address for your account.');
    }

    private function checkError($msg, $errorMessage) {

	$response = \Email\MailJet\Response::fromJsonMsg($msg, $this->failedRecipients);

	$this->assertEquals(0, $response->getStatus());
	$this->assertNotEmpty($this->failedRecipients);
	$this->assertEquals($errorMessage, $this->failedRecipients[0]);
    }

}

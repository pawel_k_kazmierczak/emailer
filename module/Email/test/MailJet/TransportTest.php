<?php

namespace EmailTest\MailJet;

class TransportTest extends \ApplicationTest\CommonTest {

    public function test_email_send_mail_by_swift() {
	$transport = (new \Email\MailJet\Transport($this->sm));

	$mailer = new \Swift_Mailer($transport);

	$message = (new \Swift_Message('Wonderful Subject'))
		->setFrom(['kontakt@pastmo.pl' => 'pastmo.pl'])
		->setTo(['pawel.rymsza@pastmo.pl' => 'Customer'])
		->setBody('Here is the message itself')
	;

	$result = $mailer->send($message, $failedRecipients);

	$this->assertEquals(1, $result);
	$this->assertEmpty($failedRecipients);
    }

}

<?php

namespace EmailTest\MailJet;

class MessageTest extends \ApplicationTest\CommonTest {

    public function test_fromSwiftMessage() {
	$result = \Email\MailJet\Message::fromSwiftMessage((new \Swift_Message('Wonderful Subject'))
				->setFrom(['kontakt@pastmo.pl' => 'From name'])
				->setTo(['to_email@customer.com' => 'Email To Name'])
				->setBody('Message body'));


	$this->assertEquals('kontakt@pastmo.pl', $result[0]->From['Email']);
	$this->assertEquals('From name', $result[0]->From['Name']);
	$this->assertEquals('to_email@customer.com', $result[0]->To[0]['Email']);
	$this->assertEquals('Email To Name', $result[0]->To[0]['Name']);
	$this->assertEquals('Wonderful Subject', $result[0]->Subject);
	$this->assertEquals('Message body', $result[0]->HTMLPart);
    }

}

<?php

namespace EmailTest\MailJet;

class TransportMockTest extends \ApplicationTest\CommonTest {

    public function test_transport_error() {
	$transport = $this->createTransportMock();
	$transport->method('execute')
		->will($this->throwException(new \Email\Exception\CurlException('Server has gone away')));

	$transport->start();
	$result = $transport->send((new \Swift_Message('Wonderful Subject')), $failedRecipients);

	$this->assertEquals(0, $result);
	$this->assertEquals(1, count($failedRecipients));
	$this->assertEquals('Server has gone away', $failedRecipients[0]);
    }

    public function test_isStarted() {
	$transport = $this->createTransportMock();
	$this->assertFalse($transport->isStarted());

	$transport->start();
	$this->assertTrue($transport->isStarted());

	$transport->stop();
	$this->assertFalse($transport->isStarted());
    }

    public function test_stop_before_start() {
	$transport = $this->createTransportMock();
	$transport->stop();

	$this->assertFalse($transport->isStarted());
    }

    private function createTransportMock() {
	return $this->getMockBuilder(\Email\MailJet\Transport::class)
			->setConstructorArgs([$this->sm])
			->setMethods(['execute'])->getMock();
    }

}

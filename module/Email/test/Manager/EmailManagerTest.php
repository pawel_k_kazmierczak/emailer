<?php

namespace EmailTest\Manager;

class EmailManagerTest extends \ApplicationTest\CommonTest {

    public function test_sendMessage_success() {
	$this->createManager(['doSend', 'parseParameters']);
	$this->emailManager->method('doSend')
		->willReturn(42);

	$result = $this->emailManager->sendMessage([]);
	$this->assertTrue($result->success);
	$this->assertEquals('42 emails has been sent', $result->getMsgs()[0]);
    }

    public function test_sendMessage_errors() {
	$this->createManager(['parseParameters']);
	$this->emailManager->method('parseParameters')
		->will($this->throwException(new \Email\Exception\ParseEmailException('parse error')));

	$result = $this->emailManager->sendMessage([]);
	$this->assertFalse($result->success);
	$this->assertEquals('parse error', $result->getMsgs()[0]);
    }

    public function testparseParameters() {
	$this->createManager();

	$post = $this->arrayToParameters([
		'from' => 'Kontakt <kontakt@pastmo.pl>',
		'to' => 'Klient <klient@abc.pl>',
		'title' => 'Tytul',
		'content' => 'Wiadomość do klienta']);
	$this->emailManager->parseParameters($post);

	$this->assertEquals('Tytul', $this->emailManager->getTitle());
	$this->assertEquals('Wiadomość do klienta', $this->emailManager->getContent());
	$this->assertEquals(['kontakt@pastmo.pl' => 'Kontakt'], $this->emailManager->getFrom());
	$this->assertEquals(['klient@abc.pl' => 'Klient'], $this->emailManager->getTo());
    }

    public function test_parseEmail_error() {
	try {
	    $this->createManager();
	    $this->emailManager->_parseEmail("wrong adres email");
	    $this->fail('No exception');
	} catch (\Email\Exception\AbstractEmailException $e) {
	    $this->assertContains('Validation failed for', $e->getMessage());
	}
    }

    public function test_checkFailedRecipients() {
	try {
	    $this->createManager();
	    $this->emailManager->resetResul();

	    $this->emailManager->checkFailedRecipients(["error"]);

	    $this->fail('No exception');
	} catch (\Email\Exception\AbstractEmailException $e) {
	    $this->assertContains('Some errors occurrence', $e->getMessage());
	}
    }

    public function test_checkStatus() {
	try {
	    $this->createManager();
	    $this->emailManager->resetResul();

	    $this->emailManager->checkStatus(0);

	    $this->fail('No exception');
	} catch (\Email\Exception\AbstractEmailException $e) {
	    $this->assertContains('No emails send', $e->getMessage());
	}
    }

    private function createManager(array $methods = ['doSend']) {
	$this->emailManager = $this->getMockBuilder(\Email\Manager\EmailManager::class)
			->setConstructorArgs([$this->sm])
			->setMethods($methods)->getMock();
    }

}
